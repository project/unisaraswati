UniSaraswati Devnagari Transliterator 
=======================================  

This module allows users to type devnagari characters while  creating Drupal 
nodes or setting the site name or menu names.   

It incorporates UniSaraswati marathi.js script developed by Prasad Shirgaonkar.  
The module creates a block, in which there are options for selecting the input method. 
  
This block should be automatically turned on.   
It is set by default to appear only in the editing mode, i.e.  
when you add a node, edit a node, or edit something in admin panel.    

To modify this configuration, go to admin/block and change the block's configuration.  

adapted for Drupal by Tushar Joshi, Nagpur <tusharvjoshi@gmail.com>
updated for Drupal 6.x by Vinay Yadav, Nagpur <vinayras@gmail.com>
Drupal 6.x code cleanup by Atul Bhosale, Pune <atul.bhosale@gmail.com>